package g30122.bia.beniamin.l4.e5.Ex5;

public class Book {
	private String name;
	private Author author;
	private double price;
	private int qtyInStock = 0;
	
	public Book(String name, Author author, double price)
	{
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public Book(String name, Author author, double price, int qtyInStock)
	{
		this.name = name;
		this.author = author;
		this.price = price;
		qtyInStock = this.qtyInStock;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Author getAuthor()
	{
		return this.author;
	}
	
	
	public double getPrice()
	{
		return this.price;
	}
	
	public double getQtyInStock()
	{
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock = qtyInStock;
	}
	
}
