package g30122.bia.beniamin.l4.e3.Ex3;

public class Circle {
	private double radius = 1.0;
	private String color = "red";
	
	public Circle()
	{
		
	}
	
	public Circle(double radius)
	{
		this.radius = radius;
	}
	
	public double getRadius()
	{
		return this.radius;
	}
	
	public double getArea()
	{
		return 2 * Math.PI * (this.radius * this.radius);
	}
}
