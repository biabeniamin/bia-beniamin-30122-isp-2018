package g30122.bia.beniamin.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class Ex7 {
	public static void Do()
	{
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();
		
    	int n = random.nextInt();
    	
    	for(int i = 0; i < 3; i++)
    	{
    		int userValue = scanner.nextInt();
    		
    		if(n == userValue)
    		{
    			System.out.println("You won!");
    			return;
    		}
    	}
    	
    	System.out.println("You lost!");
    		
    	
	}
}
