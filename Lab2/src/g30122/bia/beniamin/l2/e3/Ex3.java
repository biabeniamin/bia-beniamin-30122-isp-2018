package g30122.bia.beniamin.l2.e3;
import java.util.Scanner;

public class Ex3 {
	private static boolean IsPrim(int number)
	{
		for(int i = 2; i < number / 2; i++)
		{
			if(0 == number % i)
			{
				return false;
			}
		}
		
		return true;
	}
	public static void Do()
	{
		Scanner scanner = new Scanner(System.in);
    	
		int primeCount = 0;
		
    	int x = scanner.nextInt();
    	int y = scanner.nextInt();
    	
    	for(int i = x; i < y; i++)
    	{
    		System.out.println(i);
    		
    		if(IsPrim(i)) 
    		{
    			primeCount++;
    		}
    	}
    	
    	System.out.println("Are " + primeCount + " prime numbers");
	}
}
