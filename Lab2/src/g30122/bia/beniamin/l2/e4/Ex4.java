package g30122.bia.beniamin.l2.e4;
import java.util.Scanner;

public class Ex4 {
	public static void Do()
	{
		Scanner scanner = new Scanner(System.in);
    	
		int[] elements = new int[100];
		int maxim = 0;
		
    	int n = scanner.nextInt();
    	
    	for(int i = 0; i < n; i++)
    	{
    		System.out.println("x["+ i + "]=");
    		elements[i] = scanner.nextInt();
    		
    		if(0 == i)
    		{
    			maxim = elements[0];
    		}
    		else if(maxim < elements[i])
    		{
    			maxim = elements[i];
    		}
    	}
    	
    	System.out.println("number maxim: " + maxim);
    	
    	
	}
}
