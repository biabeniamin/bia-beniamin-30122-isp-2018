package g30122.bia.beniamin.l2.e5;
import java.util.Random;
import java.util.Scanner;

public class Ex5 {
	public static void Do()
	{
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();
    	
		int[] elements = new int[100];
		
    	int n = 10;
    	
    	for(int i = 0; i < n; i++)
    	{
    		elements[i] = random.nextInt();
    	}
    	
    	for(int i = 0; i < n; i++)
    	{
    		for(int j = i + 1; j < n; j++)
    		{
    			if(elements[i] > elements[j])
    			{
    				int aux = elements[i]; 
    				elements[i] = elements[j];
    				elements[j] = aux;
    			}
    		}
    	}
    	
    	for(int i = 0; i < n; i++)
    	{
    		System.out.print(Integer.toString(elements[i]) + " ");
    	}
    	
    	//System.out.println("number maxim: " + maxim);
    	
    	
	}
}
