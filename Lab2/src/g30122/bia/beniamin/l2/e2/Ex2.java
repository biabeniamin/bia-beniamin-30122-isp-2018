package g30122.bia.beniamin.l2.e2;
import java.util.Scanner;

public class Ex2 {
	
	public static void Do()
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("X=");
		int x = scanner.nextInt();
		
		switch(x)
		{
		case 1:
			System.out.println("One");
			break;
		case 2:
			System.out.println("Two");
			break;
		case 3:
			System.out.println("Three");
			break;
		default:
			System.out.println("Not implemented!");
			break;
		}
		
		if(1== x)
		{
			System.out.println("One");
		}
		else if(2 == x)
		{
			System.out.println("Two");
		}
		else
		{
			System.out.println("Not implemented!");
		}
	}

}
