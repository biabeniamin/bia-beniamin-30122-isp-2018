package g30122.bia.beniamin.l2.e6;

import java.util.Random;
import java.util.Scanner;

public class Ex6 {
	private static int FactRecursive(int n)
	{
		if(2 > n)
		{
			return 1;
		}
		
		return FactRecursive(n-1) * n;
	}
	
	private static int FactDynamic(int n)
	{
		int result = 1;
		
		for(int i = 2; i <= n ;i++)
		{
			result *= i;
		}
		
		return result;
	}
	
	public static void Do()
	{
		Scanner scanner = new Scanner(System.in);
			
		int n = scanner.nextInt();
    	
		System.out.println("Fact recursive: " + FactRecursive(n));
		System.out.println("Fact dynamic: " + FactDynamic(n));
    	
	}
}
