package g30122.bia.beniamin.l2.e1;
import java.util.Scanner;

public class Ex1
{
	public static void Do()
	{
		Scanner scanner = new Scanner(System.in);
    	
    	int x = scanner.nextInt();
    	int y = scanner.nextInt();
    	
    	if(x > y)
    	{
    		System.out.println("First is bigger");
    	}
    	else
    	{
    		System.out.println("Second is bigger");
    	}
	}
}
