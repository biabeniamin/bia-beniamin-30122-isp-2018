package g30122.bia.beniamin.l3.e1;
import java.util.Scanner;
import becker.robots.*;

public class Ex1
{
	public static void Do()
	{
		// Set up the initial situation
	   	City prague = new City();
	      Thing parcel = new Thing(prague, 1, 2);
	      Robot karel = new Robot(prague, 1, 0, Direction.EAST);
	 
			// Direct the robot to the final situation
	      karel.move();
	      karel.move();
	      karel.pickThing();
	      karel.move();
	      karel.turnLeft();	// start turning right as three turn lefts
	      karel.turnLeft();
	      karel.turnLeft();	// finished turning right
	      karel.move();
	      karel.putThing();
	      karel.move();
	}
}
