package g30122.bia.beniamin.l3.e4;
import java.util.Scanner;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Ex4 {
	public static void Do()
	{
		// Set up the initial situation
	      City ny = new City();
	      Robot ann = new Robot(ny, 0, 2, Direction.WEST);
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.NORTH);
	      Wall blockAve1 = new Wall(ny, 1, 2, Direction.NORTH);
	      
	      Wall blockAve2 = new Wall(ny, 1, 2, Direction.EAST);
	      Wall blockAve3 = new Wall(ny, 2, 2, Direction.EAST);
	      
	      Wall blockAve4 = new Wall(ny, 2, 2, Direction.SOUTH);
	      Wall blockAve5 = new Wall(ny, 2, 1, Direction.SOUTH);
	      
	      Wall blockAve6 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve7 = new Wall(ny, 1, 1, Direction.WEST);
	 
	 
	      // ann goes to WEST spot
	      for(int i = 0; i < 2; i++)
	    	  ann.move();
	      
	      //goes to south
	      ann.turnLeft();
	      	      
	      for(int i = 0; i < 3; i++)
	    	  ann.move();
	      
	      //goes to east point
	      ann.turnLeft();
	      
	      for(int i = 0; i < 3; i++)
	    	  ann.move();
	      
	      //north point
	      ann.turnLeft();
	      
	      for(int i = 0; i < 3; i++)
	    	  ann.move();

	    //initial point
	      ann.turnLeft();
	      
	      for(int i = 0; i < 1; i++)
	    	  ann.move();
    	
	}
}
