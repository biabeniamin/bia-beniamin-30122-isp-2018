package g30122.bia.beniamin.l3.e6;

public class MyPoint {

	private int _x;
	private int _y;
	
	public int getX()
	{
		return _x;
	}
	
	public void setX(int x)
	{
		_x = x;
	}
	
	public int getY()
	{
		return _y;
	}
	
	public void setY(int y)
	{
		_y = y;
	}
	
	public void setXY(int x, int y)
	{
		setX(x);
		setY(y);
	}
	
	public MyPoint()
	{
		_x = 0;
		_y = 0;
	}
	
	public MyPoint(int x, int y)
	{
		_x = x;
		_y = y;
	}
	
	public double  Distance(int x, int y)
	{
		return Math.sqrt(Math.pow(x - _x, 2) + Math.pow(y - _y, 2));
	}
	
	public double Distance(MyPoint point)
	{
		return Distance(point.getX(), point.getY());
	}
	
	public String toString()
	{
		return "x={" + Integer.toString(_x) + "}  y={" + Integer.toString(_y) + "}";
	}
}
