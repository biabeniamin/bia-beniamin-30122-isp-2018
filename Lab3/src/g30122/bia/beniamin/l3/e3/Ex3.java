package g30122.bia.beniamin.l3.e3;
import java.util.Scanner;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Ex3 {

	public static void Do()
	{
		// Set up the initial situation
	      City ny = new City();
	      Robot ann = new Robot(ny, 1, 1, Direction.NORTH);
	 
	 
	      // ann goes to north spot
	      for(int i = 0; i < 5; i++)
	    	  ann.move();
	      
	      for(int i = 0; i < 2; i++)//return
	    	  ann.turnLeft();
	      
	      
	      //ann return
	      for(int i = 0; i < 5; i++)
	    	  ann.move();
	      
	}
}
