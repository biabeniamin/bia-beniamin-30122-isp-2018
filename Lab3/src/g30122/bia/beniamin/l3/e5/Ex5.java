package g30122.bia.beniamin.l3.e5;
import java.util.Random;
import java.util.Scanner;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class Ex5 {
	public static void Do()
	{
		// Set up the initial situation
	      City ny = new City();
	      Robot ann = new Robot(ny, 1, 2, Direction.SOUTH);
	      Thing parcel = new Thing(ny, 2, 2);
	      
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.NORTH);
	      Wall blockAve1 = new Wall(ny, 1, 2, Direction.NORTH);
	      
	      Wall blockAve2 = new Wall(ny, 1, 2, Direction.EAST);
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.SOUTH);
	      
	      //Wall blockAve4 = new Wall(ny, 2, 2, Direction.SOUTH);
	      Wall blockAve5 = new Wall(ny, 2, 1, Direction.SOUTH);
	      
	      Wall blockAve6 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve7 = new Wall(ny, 1, 1, Direction.WEST);
	 
	      //get newspapper
	      {
		      // dog rotate
		      for(int i = 0; i < 3; i++)
		    	  ann.turnLeft();
		      
		      //move one position
		      ann.move();
		      
		      //rotate
		      ann.turnLeft();
		      
		      ann.move();
		      
		      ann.turnLeft();
		      
		      ann.move();
	      }
	      
	      ann.pickThing();
	      
	      //return 
	      {
	    	  for(int i = 0; i < 2; i++)
		    	  ann.turnLeft();
	    	  
	    	  ann.move();
		      
	    	  for(int i = 0; i < 3; i++)
		    	  ann.turnLeft();
	    	  
	    	  ann.move();
	    	  
	    	  for(int i = 0; i < 3; i++)
		    	  ann.turnLeft();
	    	  
	    	  ann.move();
	    	  
	    	  for(int i = 0; i < 3; i++)
		    	  ann.turnLeft();
	      }
	      	      
	          	
	}
}
