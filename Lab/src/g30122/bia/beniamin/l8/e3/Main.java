
package g30122.bia.beniamin.l8.e3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		int appearances = 0;
		String file;
		int command = 0;
		String fileContent = "";
		Scanner s = new Scanner(System.in);
		
		System.out.println("insert file name:");
		file = s.next();
		System.out.println("insert operation, 0 decrypt 1 enctypt:");
		command = s.nextInt();
		
		FileReader in = new FileReader(file  + "." + (command == 0 ? "enc":"dec"));
		BufferedReader buffer = new BufferedReader(in);
		
		while(true)
		{
			String line = buffer.readLine();
			if(null == line)
			{
				break;
			}
			fileContent = fileContent + line;
		}
		
		if(0 == command)
		{
			for(int i=0; i < fileContent.length(); i++ )
			{
				fileContent = fileContent.substring(0, i) + (char)(fileContent.charAt(i) >> 1) + fileContent.substring(i + 1); 
			}
		}
		else if(1 == command)
		{
			for(int i=0; i < fileContent.length(); i++ )
			{
				fileContent = fileContent.substring(0, i) + (char)(fileContent.charAt(i) << 1) + fileContent.substring(i + 1); 
			}
		}
		
		FileOutputStream out = new FileOutputStream(file  + "." + (command == 1 ? "enc":"dec"));
		for(int i=0; i < fileContent.length(); i++ )
		{
			out.write(fileContent.charAt(i));
		}
		out.close();
		
		in.close();
		
		System.out.println("Result " + fileContent);
		
	}

}
