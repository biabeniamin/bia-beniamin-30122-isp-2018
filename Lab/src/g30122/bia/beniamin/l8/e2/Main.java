package g30122.bia.beniamin.l8.e2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		int appearances = 0;
		Scanner s = new Scanner(System.in);
		
		System.out.println("insert value:");
		char value = s.next().charAt(0);
		
		FileInputStream in = new FileInputStream("data.in");
		char c;
		while(0 < in.available()) //!= EOF
		{
			c = (char) in.read();
			if(c == value)
			{
				appearances++;
			}
		}
		
		in.close();
		
		System.out.println("Appears " + appearances + " times!");
		
	}

}
