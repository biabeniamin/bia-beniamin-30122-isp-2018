package g30122.bia.beniamin.l8.e1;

 

 
class Cofee{
      private int temp;
      private int conc;
 
      Cofee(int t,int c){temp = t;conc = c;}
      int getTemp(){return temp;}
      int getConc(){return conc;}
      public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]";}
}//.class
 

 
class TemperatureException extends Exception{
      int t;
      public TemperatureException(int t,String msg) {
            super(msg);
            this.t = t;
      }
 
      int getTemp(){
            return t;
      }
}//.class
 
class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
          super(msg);
          this.c = c;
    }

    int getConc(){
          return c;
    }
}//.class

class CofeeCount extends Exception{
    public CofeeCount(String msg) {
          super(msg);
    }

}//.class