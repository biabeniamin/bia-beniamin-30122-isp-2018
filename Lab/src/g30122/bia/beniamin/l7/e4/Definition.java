package g30122.bia.beniamin.l7.e4;

public class Definition {
	String description;

	public Definition(String description) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
