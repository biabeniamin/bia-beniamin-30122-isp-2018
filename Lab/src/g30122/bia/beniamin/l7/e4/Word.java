package g30122.bia.beniamin.l7.e4;

public class Word {
	String name;

	public String getName() {
		return name;
	}

	public Word(String name) {
		super();
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
