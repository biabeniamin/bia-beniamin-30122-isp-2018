package g30122.bia.beniamin.l7.e4;

import java.util.ArrayList;
import java.util.HashMap;

public class Dictionary {
	HashMap<Word, Definition> words;
	
	public Dictionary()
	{
		words = new HashMap<Word, Definition>();
	}
	
	public void addWord(Word w, Definition d)
	{
		words.put(w, d);
	}
	
	public Definition getDefinition(Word w)
	{
		return words.get(w);
	}
	
	public ArrayList<Word> getAllWords()
	{
		return new ArrayList<Word>(words.keySet());
	
	}
	
	public ArrayList<Definition> getAllDefinitions()
	{
		return new ArrayList<Definition>(words.values());
	
	}
}
