package g30122.bia.beniamin.l7.e2;

import java.util.ArrayList;
import java.util.Comparator;

public class Bank {
	
	ArrayList<BankAccount> accounts;
	
	public Bank()
	{
		accounts = new ArrayList<BankAccount>();
	}
	
	public void addAcount(String owner, double balance)
	{
		BankAccount b = new BankAccount(owner, balance);
		accounts.add(b);
	}
	public void printAccounts()
	{
		accounts.sort(new Comparator<BankAccount>() 
		{
			@Override
			public int compare(BankAccount o1, BankAccount o2) {
				if(o1.getBalance() < (o2.getBalance()))
					return -1;
				return 1;
			}
		});
		for(BankAccount b : accounts)
		{
			System.out.println(b.getOwner() + " " + b.getBalance());
		}
		System.out.println();
	}
	public void printAccounts(double minBalance, double maxBalance)
	{
		for(BankAccount b : accounts)
		{
			if(b.getBalance() < minBalance || 
					b.getBalance() > maxBalance)
				continue;
			System.out.println(b.getOwner() + " " + b.getBalance());
		}
		System.out.println();
	}
	
	public ArrayList<BankAccount> getAllAccounts()
	{
		return accounts;
	}
	public BankAccount getAccount(String owner)
	{
		for(BankAccount b : accounts)
		{
			if(b.getOwner().equals(owner))
				return b;
		}
		
		return null;
	}
}
