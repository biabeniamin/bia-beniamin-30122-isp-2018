package g30122.bia.beniamin.l7.e2;

import java.util.ArrayList;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		Bank bank = new Bank();
		bank.addAcount("asdas1", 5);
		bank.addAcount("asdas2", 2);
		bank.addAcount("asdas3", 9);
		bank.addAcount("asdas4", 1);
		bank.printAccounts();
		bank.printAccounts(5,9);
		
		ArrayList<BankAccount> acc =  (ArrayList<BankAccount>)bank.getAllAccounts().clone();
		acc.sort(new Comparator<BankAccount>() 
		{
			@Override
			public int compare(BankAccount o1, BankAccount o2) {
				return o1.getOwner().compareTo(o2.getOwner());
			}
		});
		for(BankAccount b : acc)
		{
			System.out.println(b.getOwner() + " " + b.getBalance());
		}
		System.out.println();
		
		BankAccount b2 = bank.getAccount("asdas1");
		System.out.println(b2.getOwner() + " " + b2.getBalance());

	}

}
