package g30122.bia.beniamin.l7.e3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

public class Bank {
	
	TreeSet<BankAccount> accounts;
	
	public Bank()
	{
		accounts = new TreeSet<BankAccount>(new Comparator<BankAccount>() {
			@Override
			public int compare(BankAccount o1, BankAccount o2) {
				return o1.compareTo(o2);
			}
		});
	}
	
	public void addAcount(String owner, double balance)
	{
		BankAccount b = new BankAccount(owner, balance);
		accounts.add(b);
	}
	public void printAccounts()
	{
		for(BankAccount b : accounts)
		{
			System.out.println(b.getOwner() + " " + b.getBalance());
		}
		System.out.println();
	}
	public void printAccounts(double minBalance, double maxBalance)
	{
		for(BankAccount b : accounts)
		{
			if(b.getBalance() < minBalance || 
					b.getBalance() > maxBalance)
				continue;
			System.out.println(b.getOwner() + " " + b.getBalance());
		}
		System.out.println();
	}
	
	public ArrayList<BankAccount> getAllAccounts()
	{
		ArrayList<BankAccount> list = new ArrayList<BankAccount>();
		list.addAll(accounts);
		return list;
	}
	public BankAccount getAccount(String owner)
	{
		for(BankAccount b : accounts)
		{
			if(b.getOwner().equals(owner))
				return b;
		}
		
		return null;
	}
}
