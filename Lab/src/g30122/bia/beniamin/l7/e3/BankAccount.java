package g30122.bia.beniamin.l7.e3;



public class BankAccount implements Comparable<BankAccount> {
	String owner;
	double balance;
	
	public void withdraw(double amount)
	{
		
	}
	
	public void deposit(double amount)
	{
		
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj instanceof BankAccount)
		{
			BankAccount b2 = (BankAccount)obj;
			return b2.getOwner().equals(owner) && (b2.getBalance() == balance);
		}
		return false;
		
	}
	
	public BankAccount(String owner, double balance) {
		super();
		this.owner = owner;
		this.balance = balance;
	}

	public String getOwner() {
		return owner;
	}

	public double getBalance() {
		return balance;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return (int)balance;
	}

	@Override
	public int compareTo(BankAccount o) {
		if(getBalance() < (o.getBalance()))
			return -1;
		return 1;
	}

}
