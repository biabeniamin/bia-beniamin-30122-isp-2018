package g30122.bia.beniamin.l9.e2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;
 
public class Main extends JFrame{
 
 
      JLabel user;
      JButton bLoghin;
      private int value;
 
      Main(){
 
 
            setTitle("Test");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(200,250);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;
 
            user = new JLabel("Value: 0");
            user.setBounds(10, 50, width, height);
 
            
 
            bLoghin = new JButton("Increment");
            bLoghin.setBounds(10,150,120, height);
            bLoghin.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					value++;
					user.setText("Value: " + value);
					
				}
			});
 
            add(user);
            add(bLoghin);
 
      }
 
      public static void main(String[] args) {
            new Main();
      }
}
