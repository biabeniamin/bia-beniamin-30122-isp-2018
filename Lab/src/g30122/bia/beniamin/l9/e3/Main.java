package g30122.bia.beniamin.l9.e3;


import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;
import java.util.*;
 
public class Main extends JFrame{
 
 
      JTextField user;
      JButton bLoghin;
      JTextArea tArea;
      private int value;
 
      Main(){
 
 
            setTitle("Test");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(300,250);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;
 
            user = new JTextField("data.in");
            user.setBounds(10, 140, width, height);
 
            
 
            bLoghin = new JButton("Read file");
            bLoghin.setBounds(10,170,120, height);
            bLoghin.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					FileReader in;
					StringBuilder builder = new StringBuilder();
					try {
						in = new FileReader(user.getText());
						BufferedReader buffer = new BufferedReader(in);
						while(true)
						{
							String line = buffer.readLine();
							if(null == line)
							{
								break;
							}
							builder.append(line + "\n");
						}
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					tArea.setText(builder.toString());
					
					
					
					
				}
			});
            
            tArea = new JTextArea();
            tArea.setBounds(20, 20, 180, 100);
 
            add(user);
            add(bLoghin);
            add(tArea);
 
      }
 
      public static void main(String[] args) {
            new Main();
      }
}
