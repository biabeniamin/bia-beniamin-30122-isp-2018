package g30122.bia.beniamin.l9.e4;

import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import java.util.*;
 
public class Main extends JFrame{
 
 
      JButton bLoghin[][] = new JButton[3][3];
      private boolean round = false;
      private int value;
 
      Main(){
 
 
            setTitle("Test");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(300,250);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;
 

            for(int i=0;i<3;i++)
            {
            	for(int j=0;j<3;j++)
            	{
            		 bLoghin[i][j] = new JButton("");
                     bLoghin[i][j].setBounds(i * 50,j * 50,50, 50);
                     bLoghin[i][j].addActionListener(new ActionListener() {
         				
         				@Override
         				public void actionPerformed(ActionEvent e) {
         					JButton button = ((JButton)e.getSource());
         					buttonClick(button.getY()/50, (int)button.getX()/50);
         				}
         			});
                    
                     add(bLoghin[i][j]);
            	}
            }
 
      }
      
      private boolean IsZeroRound()
      {
    	  return round;
      }
      
      private int GetCellValue(int x, int y)
      {
    	  if(0 > x || 2 < x )
    	  {
    		  return -1;
    	  }
    	  
    	  if(0 > y || 2 < y )
    	  {
    		  return -1;
    	  }
    	  
    	  if(bLoghin[x][y].getText().equals(""))
    	  {
    		  return -1;
    	  }
    	  
    	  if(bLoghin[x][y].getText().equals("X"))
    	  {
    		  return 1;
    	  }
    	  
    	  return 0;
      }
      
      private void CheckWinning()
      {
    	  for(int i=0;i<3;i++)
          {
          	for(int j=0;j<3;j++)
          	{
          		if((GetCellValue(i, j) == GetCellValue(i - 1, j)) && 
          				(GetCellValue(i, j) == GetCellValue(i + 1, j))
          				|| (GetCellValue(i, j) == GetCellValue(i - 1, j-1)) && 
          				(GetCellValue(i, j) == GetCellValue(i + 1, j + 1))
          				|| (GetCellValue(i, j) == GetCellValue(i - 1, j+1)) && 
          				(GetCellValue(i, j) == GetCellValue(i + 1, j - 1))
          				|| (GetCellValue(i, j) == GetCellValue(i, j+1)) && 
          				(GetCellValue(i, j) == GetCellValue(i, j - 1)))
          		{
          			if((GetCellValue(i, j) == -1))
          					continue;
          			JOptionPane.showMessageDialog(this,"Win! " + (IsZeroRound() ? "Zero":"X") + " won!");	
          		}
          	}
          }
      }
      
      private void buttonClick(int i, int j)
      {
    	  JButton button = bLoghin[j][i];
    	  if(button.getText().equals(""))
    	  {
    		  button.setText(IsZeroRound() ? "0" : "X");
    		  CheckWinning();
    		  round = !round;
    	  }
    	  else
    	  {
    		  JOptionPane.showMessageDialog(this,"Already pressed!");
    	  }
    	  System.out.println(i + " "+ j);
      }
 
      public static void main(String[] args) {
            new Main();
      }
}
