package g30122.bia.beniamin.test3;
public class Book
{
	private int nrOfPages;
	
	public Book(int nrOfPages)
	{
		this.nrOfPages = nrOfPages;
	}
	
	public void read()
	{
		System.out.println("Reading a Book");
	}
}
