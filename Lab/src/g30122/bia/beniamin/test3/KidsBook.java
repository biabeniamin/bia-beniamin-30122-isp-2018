package g30122.bia.beniamin.test3;
public class KidsBook extends Book
{

	public KidsBook(int nrOfPages)
	{
		super(nrOfPages);
	}
	
	public void read()
	{
		System.out.println("Reading an KidsBook");
	}

}
