package g30122.bia.beniamin.test3;


public class ActionBook extends Book
{

	public ActionBook(int nrOfPages)
	{
		super(nrOfPages);
	}
	
	public void read()
	{
		System.out.println("Reading an ActionBook");
	}

}
