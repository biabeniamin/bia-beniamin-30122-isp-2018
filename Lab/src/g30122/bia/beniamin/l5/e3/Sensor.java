package g30122.bia.beniamin.l5.e3;

public abstract class Sensor {
	String location;

	public String getLocation() {
		return location;
	}
	
	public abstract int readValue();

}
