package g30122.bia.beniamin.l5.e3;

import java.util.Random;

public class TemperatureSensor extends Sensor
{

	@Override
	public int readValue() {
		return new Random().nextInt(100);
	}

}
