package g30122.bia.beniamin.l5.e3;

public class Controller {
	private TemperatureSensor temperatureSensor;
	private LightSensor lightSensor;
	private static Controller instance;
	
	private Controller()
	{
		temperatureSensor = new TemperatureSensor();
		lightSensor = new LightSensor();
	}
	
	public void control()
	{
		for(int i=0;i<20;i++)
		{
			System.out.println("Temp=" + temperatureSensor.readValue() + 
					"Light=" + lightSensor.readValue());
			try
			{
				Thread.sleep(1000);
			}
			catch(Exception e)
			{
				
			}
		}
	}
	
	public static Controller GetInstance()
	{
		if(null == instance)
		{
			instance = new Controller();
		}
		
		return instance;
	}
}
