package g30122.bia.beniamin.l5.e1;

class Rectangle extends Shape{

	   protected double width;
	   protected double length;
	   
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}


	

	   public Rectangle(double width, double lenght){

		   this.width = width;
		   this.length = lenght;
	   }


	   public Rectangle(String color, boolean filled,double width, double lenght) {
	       super(color, filled);
	       this.width = width;
		   this.length = lenght;
	   }

	   @Override
	   public double getArea() {
	       return length * width; //replace with formula for get area
	   }

	   @Override
	   public double getPerimeter() {
	       return length + width; //replace with formula for get perimeter
	   }


	   @Override
	   public String toString() {
	       return "Rectangle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               '}';
	   }
	}
