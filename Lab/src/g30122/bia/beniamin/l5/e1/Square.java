package g30122.bia.beniamin.l5.e1;

public class Square extends Rectangle {

	public Square(String color, boolean filled, double side) {
		super(color, filled, side, side);
		
	}
	
	public double getSide()
	{
		return width;
	}
	
	public void setSide(double side)
	{
		width = side;
	}
	
	@Override
	public String toString() {
	       return "Square{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               '}';
	   }

}
