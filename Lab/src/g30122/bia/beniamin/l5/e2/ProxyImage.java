package g30122.bia.beniamin.l5.e2;

public class ProxyImage implements Image{
	 
	   private RealImage realImage;
	   private String fileName;
	 
	   public ProxyImage(String fileName){
		      this.fileName = fileName;
		   }
	   
	   public ProxyImage(String fileName, boolean doRotate){
		      this.fileName = fileName;
		      
		      if(doRotate)
		      {
		    	  RotateImage();
		      }
		      else
		      {
		    	  display();
		      }
		   }
	 
	   @Override
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();
	   }

		public void RotateImage() {
			System.out.println("Rotating " + fileName);
			
		}
	}