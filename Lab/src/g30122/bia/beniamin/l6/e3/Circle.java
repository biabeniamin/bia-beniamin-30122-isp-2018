package g30122.bia.beniamin.l6.e3;


import java.awt.*;

public class Circle implements Shape{

    private int radius;
    private Color color;
	private Boolean fill = false;
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Boolean getFill() {
		return fill;
	}

	public void setFill(Boolean fill) {
		this.fill = fill;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	private String id;
	private int x = 50;
	private int y = 50;

    public Circle(Color color, int radius) {
        this.color = color;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(true == getFill())
        {
        	g.fillOval(getX(),getY(),radius,radius);
        }
        else
        {
        	g.drawOval(getX(),getY(),radius,radius);
        }
    }
}
