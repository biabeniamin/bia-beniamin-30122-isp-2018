package g30122.bia.beniamin.l6.e3;


import java.awt.*;

public class Rectangle implements Shape{

	private Color color;
	private Boolean fill = false;
	private int width;
	private int height;
	private String id;
	private int x = 50;
	private int y = 50;
    public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	

    public Rectangle(Color color, int width, int height) {
        this.color = color;
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+width+" "+getColor().toString());
        
        g.setColor(getColor());
        
        if(true == getFill())
        {
        	g.fillRect(getX(),getY(), width, height);
        }
        else
        {
        	g.drawRect(getX(),getY(), width, height);
        }
    }

	public Boolean getFill() {
		return fill;
	}

	public void setFill(Boolean fill) {
		this.fill = fill;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


}
