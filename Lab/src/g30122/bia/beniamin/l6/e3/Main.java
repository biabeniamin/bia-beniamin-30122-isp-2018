package g30122.bia.beniamin.l6.e3;

import java.awt.*;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100);
        b1.addShape(s2);
        
        Shape r1 = new Rectangle(Color.GREEN, 50,50);
        r1.setX(100);;
        r1.setId("asfas");
        r1.setFill(true);
        b1.addShape(r1);
        
        //b1.deleteById("asfas");
    }
}
