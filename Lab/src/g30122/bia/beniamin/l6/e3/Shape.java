package g30122.bia.beniamin.l6.e3;


import java.awt.*;

public interface Shape {

    
    public Boolean getFill();

	public void setFill(Boolean fill);

	public String getId();
	

	public void setId(String id);


	
    public int getX();

	public void setX(int x);

	public int getY() ;

	public void setY(int y) ;

    public Color getColor();

    public void setColor(Color color);
    

    public  void draw(Graphics g);
}
