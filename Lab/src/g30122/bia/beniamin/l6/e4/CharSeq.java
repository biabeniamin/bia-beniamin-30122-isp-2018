package g30122.bia.beniamin.l6.e4;

public class CharSeq implements CharSequence
{
	private char[] text;
	private int length;

	@Override
	public char charAt(int arg0) {
		return text[arg0];
	}

	@Override
	public int length() {
		return this.length;
	}

	@Override
	public CharSequence subSequence(int arg0, int arg1) {
		String sub = new String(text);
		return new CharSeq(sub.substring(arg0, arg1));
	}

	public CharSeq(String text) {
		super();
		this.text = text.toCharArray();
		this.length = text.length();
	}
	
	@Override
	public String toString()
	{
		return new String(text);
	}
	
	

}
