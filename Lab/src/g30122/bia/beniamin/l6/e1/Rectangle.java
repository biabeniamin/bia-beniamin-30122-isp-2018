package g30122.bia.beniamin.l6.e1;


import java.awt.*;

public class Rectangle extends Shape{

	private int width;
	private int height;

    public Rectangle(Color color, int width, int height) {
        super(color);
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+width+" "+getColor().toString());
        
        g.setColor(getColor());
        
        if(true == getFill())
        {
        	g.fillRect(getX(),getY(), width, height);
        }
        else
        {
        	g.drawRect(getX(),getY(), width, height);
        }
    }
}
