package g30122.bia.beniamin.l6.e1;


import java.awt.*;

public abstract class Shape {

    private Color color;
    private Boolean fill = false;
    public Boolean getFill() {
		return fill;
	}

	public void setFill(Boolean fill) {
		this.fill = fill;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	private String id;
    public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}


	private int x = 50;
    private int y = 50;
    

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    

    public abstract void draw(Graphics g);
}
